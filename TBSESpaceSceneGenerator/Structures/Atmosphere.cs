﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBSESpaceSceneGenerator.Structures
{
    public enum Gas
    {
        Nitrogen,
        Oxygen,
        CarbonDioxide,
        Methane,
        Argon,
        Helium,
        Hydrogen,
        Count
    }

    class Atmosphere
    {
        public Dictionary<Gas, double> Gases { get; private set; }

        public Atmosphere()
        {
            Gases = new Dictionary<Gas, double>();
        }

        public void AddGas(Gas gas, double percentage)
        {
            if (Gases.ContainsKey(gas))
                Gases[gas] = percentage;
            else
                Gases.Add(gas, percentage);
        }

        public double GetGasPercentage()
        {
            double sum = 0.0;
            foreach (KeyValuePair<Gas, double> kvp in Gases)
            {
                sum += kvp.Value;
            }
            return sum;
        }

    }
}
