﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TBSESpaceSceneGenerator.Generators;
using TBSESpaceSceneGenerator.Structures;
using TBSESpaceSceneGenerator.XML;

namespace TBSESpaceSceneGenerator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.Title += " v" + App.Version;
        }

        private void btnGenerateSpaceScene_Click(object sender, RoutedEventArgs e)
        {
            int seed, solarSystemCount;
            try
            {
                seed = Convert.ToInt32(txtSeed.Text);
                solarSystemCount = Convert.ToInt32(txtSolarSystemCount.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error parsing textbox values.\n" + ex.Message, "Parsing Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            txtOutput.Text = "Generating Universe..." + Environment.NewLine;

            List<SolarSystem> solarSystems = new UniverseGenerator().Generate(seed, solarSystemCount);

            txtOutput.Text += "Generated " + solarSystems.Count + " Solar Systems" + Environment.NewLine;

            if (chkOutputXML.IsChecked == true)
            {
                txtOutput.Text += "Generating XML..." + Environment.NewLine;

                SceneWriter.WriteUniverseFile(solarSystems);
                SceneWriter.WriteSolarSystemFiles(solarSystems);

                txtOutput.Text += "Generated XML";
            }

        }
    }
}
