﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBSESpaceSceneGenerator.Structures;

namespace TBSESpaceSceneGenerator.Generators
{
    class UniverseGenerator
    {
        public List<SolarSystem> Generate(int seed, int solarSystemCount)
        {
            List<SolarSystem> solarSystems = new List<SolarSystem>(solarSystemCount);
            SolarSystem.ResetCount();

            RandomSingleton.Instance().SetSeed(seed);

            SolarSystemGenerator solarSystemGenerator = new SolarSystemGenerator();

            Parallel.For(0, solarSystemCount, (i) =>
            {
                solarSystems.Add(solarSystemGenerator.Generate());
            });

            return solarSystems;
        }
    }
}
